package model.data_structures;

import java.util.*;

/**
 * Representa un v�rtice del grafo
 * 
 */
public class Vertice<T extends Comparable<T> , K>
{
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

	/**
     * Id del v�rtice
     */
    private K id;
	
	/**
     * Elemento contenido en el v�rtice
     */
    private T elem;

    /**
     * Lista de arcos hacia los sucesores de �ste v�rtice
     */
    private ArrayList<Arco> precedentores;

    /**
     * Lista de arcos hacia los sucesores de �ste v�rtice
     */
    private ArrayList<Arco> sucesores;

    /**
     * Marca del nodo
     */
    private boolean marcado;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Constructor del v�rtice
     * @param pElem Elemento contenido en el v�rtice
     */
    public Vertice( T pElem  )
    {
       
        sucesores = new ArrayList( );
        precedentores = new ArrayList( );
        marcado = false;
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Devuelve la informaci�n del v�rtice
     * @return Informaci�n del v�rtice
     */
    public T darInfoVertice( )
    {
        return elem;
    }

    /**
     * Devuelve los arcos hacia los sucesores del v�rtice
     * @return Arcos hacia los sucesores del v�rtice
     */
    public ArrayList< Arco> darSucesores( )
    {
        return sucesores;
    }

    /**
     * Devuelve los arcos hacia los precedentores del v�rtice
     * @return Arcos hacia los precedentores del v�rtice
     */
    public ArrayList<Arco> darprecedentores( )
    {
        return precedentores;
    }

    /**
     * Devuelve el arco (si existe) hacia el v�rtice especificado. Devuelve null si no existe.
     * @param idDestino Identificador del v�rtice destino
     * @return Arco hacia el v�rtice especificado, null si no existe
     */
    public Arco darArco( K idDestino )
    {
        // Busca secuencialmente el arco
        for( int i = 0; i < sucesores.size( ); i++ )
        {
            Arco arco = sucesores.get( i );
            if( idDestino.equals( arco.darVerticeDestino( ).darId( ) ) )
            {
                return arco;
            }
        }
        return null;
    }
    
    public K darId()
    {
    	return id;
    }

    /**
     * Devuelve la marca del v�rtice
     * @return Indica si el v�rtice se encuentra marcado
     */
    public boolean marcado( )
    {
        return marcado;
    }

    /**
     * Marca el v�rtice
     */
    public void marcar( )
    {
        marcado = true;
    }

    /**
     * Elimina la marca del v�rtice
     */
    public void desmarcar( )
    {
        marcado = false;
    }

    /**
     * Elimina un arco del v�rtice
     * @param idDestino Identificador del v�rtice destino del arco que se quiere eliminar
     * @throws ArcoNoExisteException Excepci�n generada cuando el arco no existe
     */
    public void eliminarArco( K idDestino ) throws Exception
    {
        Arco arco = darArco( idDestino );
        if( arco == null )
        {
            throw new Exception( "El arco no existe");
        }
        sucesores.remove( arco );
        arco.darVerticeDestino( ).eliminarArcoPrecedentor( arco );
    }

    /**
     * Elimina un arco de los precedentores del v�rtice
     * @param arco Arco a eliminar
     */
    private void eliminarArcoPrecedentor( Arco arco )
    {
        precedentores.remove( arco );
    }

    /**
     * Agrega un arco al v�rtice
     * @param arco Arco a agregar al v�rtice
     * @throws ArcoYaExisteException Excepci�n generada cuando ya hay un arco hacia el mismo v�rtice
     */
    public void agregarArco( Arco arco ) throws Exception
    {
        K idDestino = (K) arco.darVerticeDestino( ).darId( );
        if( esSucesor( idDestino ) )
        {
            throw new Exception( "El arco ya existe");
        }
        sucesores.add( arco );
        arco.darVerticeDestino( ).agregarArcoPredecesor( arco );
    }

    /**
     * Agrega un arco al v�rtice
     * @param arco Arco a agregar al v�rtice
     * @throws ArcoYaExisteException Excepci�n generada cuando ya hay un arco hacia el mismo v�rtice
     */
    private void agregarArcoPredecesor( Arco arco ) throws Exception
    {
        precedentores.add( arco );
    }

    /**
     * Elimina todos los arcos del v�rtice
     */
    public void eliminarArcos( )
    {
        sucesores.clear( );
    }

    /**
     * Verifica si el arco especificado es sucesor de �ste
     * @param idDestino Identificador del v�rtice destino
     * @return True si es sucesor, False si no
     */
    public boolean esSucesor( K idDestino )
    {
        return darArco( idDestino ) != null;
    }

    /**
     * Devuelve el n�mero de sucesores del v�rtice
     * @return N�mero de sucesores del v�rtice
     */
    public int darNumeroSucesores( )
    {
        return sucesores.size( );
    }

    /**
     * Devuelve el n�mero de precedentores del v�rtice
     * @return N�mero de precedentores del v�rtice
     */
    public int darNumeroprecedentores( )
    {
        return precedentores.size( );
    }

    /**
     * Indica si hay un camino simple del v�rtice actual al v�rtice que se recibe como par�metro
     * @param destino V�rtice destino de la b�squeda
     * @return True si existe, False si no
     */
    public boolean hayCamino( Vertice destino )
    {
        if( darId( ).equals( destino.darId( ) ) )
            return true;
        else
        {
            marcar( );
            for( Arco arco : darSucesores( ) )
            {
                Vertice vertice = arco.darVerticeDestino( );
                if( !vertice.marcado( ) && vertice.hayCamino( destino ) )
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Devuelve el camino mas corto al v�rtice especificado
     * @param destino V�rtice destino
     * @return Camino mas corto hacia el v�rtice especificado
     */
    public Camino darCaminoMasCorto( Vertice destino )
    {
        if( darId( ).equals( destino.darId( ) ) )
        {
            return new Camino(elem) ;
        }
        else
        {
            marcar( );
            ArrayList<Arco> sucesores = darSucesores( );
            Camino camino = null;
            Arco arcoEnCamino = null;
            for( int i = 0; i < sucesores.size( ); i++ )
            {
                Arco arco = sucesores.get( i );
                Vertice vert = arco.darVerticeDestino( );
                if( !vert.marcado( ) )
                {
                    Camino cam = vert.darCaminoMasCorto( destino );
                    if( cam != null )
                    {
                        if( camino == null || cam.darLongitud( ) < camino.darLongitud( ) )
                        {
                            camino = cam;
                            arcoEnCamino = arco;
                        }
                    }
                }
            }
            desmarcar( );
            if( camino == null )
                return null;
            else
            {
                camino.agregarArcoComienzo( darInfoVertice( ), (T)arcoEnCamino.darInfoArco( ) );
                return camino;
            }
        }
    }
    
    
    /**
     * Devuelve el camino mas barato hacia el v�rtice especificado
     * @param destino V�rtice destino
     * @return Camino mas barato al v�rtice especificado
     */
    public Camino darCaminoMasBarato( Vertice destino )
    {
        if( darId( ).equals( destino.darId( ) ) )
            return new Camino( elem );
        else
        {
            marcar( );
            ArrayList sucesores = darSucesores( );
            Camino camino = null;
            Arco arcoEnCamino = null;
            for( int i = 0; i < sucesores.size( ); i++ )
            {
                Arco arco = (Arco)sucesores.get( i );
                Vertice vert = arco.darVerticeDestino( );
                if( !vert.marcado( ) )
                {
                    Camino cam = vert.darCaminoMasBarato( destino );
                    if( cam != null )
                    {
                        if( camino == null || cam.darCosto( ) + arco.darPeso( ) < camino.darCosto( ) + arcoEnCamino.darPeso( ) )
                        {
                            camino = cam;
                            arcoEnCamino = arco;
                        }
                    }
                }
            }
            desmarcar( );
            if( camino == null )
                return null;
            else
            {
                camino.agregarArcoComienzo( darInfoVertice( ), (T)arcoEnCamino.darInfoArco( ) );
                return camino;
            }
        }
    }

    /**
     * Indica si hay un ciclo que parte del v�rtice actual
     * 
     * @return True si existe un ciclo, False si no
     */
    public boolean hayCiclo( )
    {
        ArrayList<Arco> sucesores = darSucesores( );
        for( Arco arco : sucesores )
        {
            Vertice vert = arco.darVerticeDestino( );
            if( vert.hayCamino( this ) )
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Completa un arbol parcial de recubrimiento con los v�rtices no marcados del grafo que sean sucesores de este v�rtice
     * @param arbolPR Grafo que representa el �bol parcial de recubrimiento
     */
    public void darArbolParcialRecubrimiento( GrafoDirigido arbolPR )
    {
        marcar( );
        for( Arco sucesor : sucesores )
        {
            if( !sucesor.darVerticeDestino( ).marcado( ) )
                try
                {
                    arbolPR.agregarArco( darId( ), sucesor.darVerticeDestino( ).darId( ), sucesor );
                    sucesor.darVerticeDestino( ).darArbolParcialRecubrimiento( arbolPR );
                }
                catch( Exception e )
                {
                   
                }
        }
    }

    /**
     * Marca todos los v�rtices adyacentes al v�rtice.
     */
    public void marcarAdyacentes( )
    {
        for( Arco sucesor : sucesores )
        {
            if( !sucesor.darVerticeDestino( ).marcado( ) )
            {
                sucesor.darVerticeDestino( ).marcar( );
                sucesor.darVerticeDestino( ).marcarAdyacentes( );
            }
        }
        for( Arco predecesor : precedentores )
        {
            if( !predecesor.darVerticeOrigen( ).marcado( ) )
            {
                predecesor.darVerticeOrigen( ).marcar( );
                predecesor.darVerticeOrigen( ).marcarAdyacentes( );
            }
        }
    }

}
