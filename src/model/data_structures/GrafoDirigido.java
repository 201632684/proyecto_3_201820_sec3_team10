package model.data_structures;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import model.vo.VOComponenteFuertementeConectada;


/**
 * Representa un grafo dirigido
 * 
 * @param <K> Tipo del identificador de un v�rtice
 * @param <V> Tipo de datos del elemento del v�rtice
 * @param <A> Tipo de datos del elemento del arco
 */
public class GrafoDirigido<K, V extends Vertice, A extends Arco> implements Serializable
{
    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------
	
    /**
	 * Constante para la serializaci�n
	 */
	private static final long serialVersionUID = 1L;
	
    // -----------------------------------------------------------------
    // Constantes
    // ------------------------------------------------------------------
    /**
     * Constante que representa un valor infinito
     */
    public static final int INFINITO = -1;

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Tabla de hashing con los v�rtices
     */
    private HashMap<K, Vertice> vertices;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Crea un nuevo grafo vac�o
     */
    public GrafoDirigido( )
    {
        vertices = new HashMap<K, Vertice>( );
    }
    
    public GrafoDirigido ( IGraph pGrafo)
    {
    	vertices = new HashMap<K, Vertice>( );
    	for (int i = 0; i < pGrafo.darVerices().length; i++) {
			Object[] listaVertices = pGrafo.darVerices();
    		Vertice v = (Vertice) listaVertices[i];
    		try {
    			agregarVertice((V)v);
			} catch (Exception e) {
				// TODO: handle exception
			}
    		
		}
    	for (int i = 0; i < pGrafo.darArcos().length; i++) {
			Object[] listaArcos = pGrafo.darArcos();
    		Arco a = (Arco) listaArcos[i];
    		try {
    			agregarArco((K)a.darVerticeOrigen().darId(), (K)a.darVerticeDestino().darId(), (A)a.darInfoArco());
			} catch (Exception e) {
				// TODO: handle exception
			}
    		
		}
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Indica si el v�rtice con el identificador dado existe en el grafo
     * @param idVertice Identificador del v�rtice
     * @return <code>true</code> si el v�rtice con el identificador dado existe o <code>false</code> en caso contrario
     */
    public boolean existeVertice( K idVertice )
    {
        return vertices.get( idVertice ) != null;
    }

    /**
     * Retorna los v�rtices del grafo.
     * @return Los v�rtices del grafo.
     */
    public Lista darVertices( )
    {
        // Crear la lista
        Lista vs = new Lista( );

        // Recorrer los v�rtices y poblar la lista
        for( Vertice v : vertices.values( ) )
        {
            vs.agregar( v.darInfoVertice( ) );
        }

        // Retornar la lista
        return vs;
    }

    /**
     * Devuelve todos los v�rtices del grafo.
     * </p>
     * Este m�todo retorna los objetos Vertice, propios de est� implementaci�n.
     * @return Vertices del grafo
     */
    public Collection<Vertice> darObjetosVertices( )
    {
        return vertices.values( );
    }

    /**
     * Retorna el arco entre los v�rtices ingresados por parametros
     * @param idV1 id del primer v�rtice
     * @param idV2 id del segundo v�rtice
     * @return El arco entre los v�rtices ingresados por parametros
     * @throws VerticeNoExisteException si alguno de los v�rtices ingresados por parametros no existe en el grafo
     * @throws ArcoNoExisteException si no existe un arco entre esos v�rtices
     */
    public A darArco( K idVerticeOrigen, K idVerticeDestino ) throws Exception
    {
        // Busca el primer v�rtice y luego busca el arco
        Vertice vertice = darVertice( idVerticeOrigen );
        if( existeVertice( idVerticeDestino ) )
        {
            Arco arco = vertice.darArco( idVerticeDestino );
            if( arco == null )
                throw new Exception( "No existe un arco entre los v�rtices seleccionados");
            else
                return (A)arco.darInfoArco( );
        }
        else
            throw new Exception( "V�rtice destino no existe");
    }

    /**
     * Indica si existe un arco entre los v�rtices ingresados por parametros
     * @param idV1 id del primer v�rtice
     * @param idV2 id del segundo v�rtice
     * @return <code>true</code> si existe un arco entre los v�rtices ingresado o <code>false</code> en caso contrario.
     * @throws VerticeNoExisteException si alguno de los v�rtices ingresados por parametros no existe en el grafo
     */
    public boolean existeArco( K idVerticeOrigen, K idVerticeDestino ) throws Exception
    {
        // Busca el primer v�rtice y luego busca el arco
        Vertice vertice = darVertice( idVerticeOrigen );
        if( existeVertice( idVerticeDestino ) )
            return vertice.darArco( idVerticeDestino ) != null;
        else
            throw new Exception( "V�rtice destino no existe");
    }

    /**
     * Devuelve todos los arcos del grafo
     * @return Arcos del grafo
     */
    public Lista darArcos( )
    {
        Lista arcos = new Lista( );

        // Recorre todos los v�rtices buscando los arcos
        for( Vertice vertice : vertices.values( ) )
        {
            // Recorrer los arcos del v�rtice y poblar la lista arcos
            for(Object arco : vertice.darSucesores( ) )
            	arcos.agregar((Comparable)((Arco)arco).darInfoArco( ) );
        }
        return arcos;

    }

    /**
     * Retorna todos los objetos usados para representar un arco dentro del grafo.
     * @return Todos los objetos usados para representar un arco dentro del grafo.
     */
    public ArrayList<Arco> darObjetosArco( )
    {
        ArrayList<Arco> arcos = new ArrayList<Arco>( );

        // Recorre todos los v�rtices buscando los arcos
        for( Vertice vertice : vertices.values( ) )
            arcos.addAll( vertice.darSucesores( ) );

        // Retornar la lista
        return arcos;
    }

    /**
     * Devuelve los id de los v�rtice sucedores a un v�rtice ingresado por par�metro
     * @param idVertice Identificador del v�rtice
     * @return Los id de los v�rtice sucedores a un v�rtice ingresado por par�metro
     * @throws VerticeNoExisteException Si el v�rtice especificado no existe
     */
    public Lista darSucesores( K idVertice ) throws Exception
    {
        Lista lista = new Lista( );
        for( Object a : darVertice( idVertice ).darSucesores( ) )
        {
            lista.agregar( ((Arco)a).darVerticeDestino( ).darInfoVertice( ) );
        }
        return lista;
    }

    /**
     * Devuelve los id de los v�rtice precedentores a un v�rtice ingresado por par�metro
     * @param idVertice Identificador del v�rtice
     * @return Los id de los v�rtice precedentores a un v�rtice ingresado por par�metro
     * @throws VerticeNoExisteException Si el v�rtice especificado no existe
     */
    public Lista darPrecedentores( K idVertice ) throws Exception
    {
        Lista lista = new Lista( );
        for( Object a : darVertice( idVertice ).darprecedentores( ) )
        {
            lista.agregar( ((Arco)a).darVerticeDestino( ).darInfoVertice( ) );
        }
        return lista;
    }

    /**
     * Crea un nuevo v�rtice en el grafo
     * @param elemento Elemento del v�rtice
     * @throws VerticeYaExisteException Si el v�rtice que se quiere agregar ya existe
     */
    public void agregarVertice( V elemento ) throws Exception
    {
        if( existeVertice( (K) elemento.darId( ) ) )
            throw new Exception( "Elemento ya existe");
        else
        {
            Vertice vertice = new Vertice( (Comparable) elemento);
            vertices.put( (K)elemento.darId( ), vertice );
        }
    }

    /**
     * Elimina el v�rtice identificado con el Identificador especificado
     * @param idVertice Identificador del v�rtice
     * @throws VerticeNoExisteException suando el v�rtice especificado no existe
     */
    public void eliminarVertice( K idVertice ) throws Exception
    {
        // Localiza el v�rtice en el grafo
        Vertice vertice = darObjetoVertice( idVertice );
        // Elimina todos los arcos que salen del v�rtice
        vertice.eliminarArcos( );
        // Localiza en el grafo todos los arcos que llegan a este v�rtice y los
        // elimina
        for( Vertice vert : vertices.values( ) )
        {
            try
            {
                vert.eliminarArco( vertice.darId( ) );
            }
            catch( Exception e )
            {
                // En caso de no existir no hace nada
            }
        }
        // Elimina el v�rtice
        vertices.remove( vertice.darId( ) );
    }

    /**
     * Agrega un nuevo arco al grafo
     * @param idVerticeOrigen Identificador del v�rtice desde donde sale el arco
     * @param idVerticeDestino Identificador del v�rtice hasta donde llega el arco
     * @param infoArco Elemento del arco
     * @throws VerticeNoExisteException Si alguno de los v�rtices especificados no existe
     * @throws ArcoYaExisteException Si ya existe un arco entre esos dos v�rtices
     */
    public void agregarArco( K idVerticeOrigen, K idVerticeDestino, A infoArco ) throws Exception
    {
        // Obtiene los v�rtices
        Vertice verticeOrigen = darVertice( idVerticeOrigen );
        Vertice verticeDestino = darObjetoVertice( idVerticeDestino );
        // Crea el arco y lo agrega
        Arco arco = new Arco( verticeOrigen, verticeDestino, infoArco );
        verticeOrigen.agregarArco( arco );
    }

    /**
     * Elimina el arco que existe entre dos v�rtices
     * @param idVerticeOrigen Identificador del v�rtice desde donde sale el arco
     * @param idVerticeDestino Identificador del v�rtice hasta donde llega el arco
     * @throws VerticeNoExisteException Cuando el v�rtice de salida no existe
     * @throws ArcoNoExisteException Cuando el arco no existe
     */
    public void eliminarArco( K idVerticeOrigen, K idVerticeDestino ) throws Exception
    {
        // Obtiene el v�rtice y elimina el arco
        Vertice verticeOrigen = darObjetoVertice( idVerticeOrigen );
        verticeOrigen.eliminarArco( idVerticeDestino );
    }

    /**
     * Retorna el n�mero de arcos que tiene el grafo
     * @return el n�mero de arcos que tiene el grafo
     */
    public int darNArcos( )
    {
        return darArcos( ).size();
    }

    /**
     * Devuelve el orden del grafo.
     * </p>
     * El orden de un grafo se define con el n�mero de v�rtices que tiene este
     * @return Orden del grafo
     */
    public int darOrden( )
    {
        return vertices.size( );
    }

    /**
     * Verifica si existe un camino entre los dos v�rtices especificados
     * @param idVerticeOrigen V�rtice de origen
     * @param idVerticeDestino V�rtice de destino
     * @return <code>true</code> si hay camino entre los dos v�rtices especificados o <code>false</code> de lo contrario
     * @throws VerticeNoExisteException Si no existe alguno de los dos v�rtices dados
     */
    public boolean hayCamino( K idVerticeOrigen, K idVerticeDestino ) throws Exception
    {
        // Borra todas las marcas presentes en el grafo
        reiniciarMarcas( );
        // Obtiene los v�rtices
        Vertice verticeOrigen = darObjetoVertice( idVerticeOrigen );
        Vertice verticeDestino = darObjetoVertice( idVerticeDestino );
        return verticeOrigen.hayCamino( verticeDestino );
    }

    /**
     * Retorna el camino m�s corto (de menor longitud) entre el par de v�rtices especificados
     * @param idVerticeOrigen V�rtice en el que inicia el camino
     * @param idVerticeDestino V�rtice en el que termina el camino
     * @return El camino m�s corto entre el par de v�rtices especificados
     * @throws VerticeNoExisteException Si alguno de los dos v�rtices no existe
     */
    public Camino darCaminoMasCorto( K idVerticeOrigen, K idVerticeDestino ) throws Exception
    {
        // Borra todas las marcas presentes en el grafo
        reiniciarMarcas( );
        // Obtiene los v�rtices
        Vertice verticeOrigen = darObjetoVertice( idVerticeOrigen );
        Vertice verticeDestino = darObjetoVertice( idVerticeDestino );
        // Le pide al v�rtice de origen que localice el camino
        return verticeOrigen.darCaminoMasCorto( verticeDestino );
    }
    
    /**
     * Retorna el camino m�s barato (de menor costo) entre el par de v�rtices especificados
     * @param idVerticeOrigen V�rtice en el que inicia el camino
     * @param idVerticeDestino V�rtice en el que termina el camino
     * @return El camino m�s barato entre el par de v�rtices especificados
     * @throws VerticeNoExisteException Si alguno de los dos v�rtices no existe
     */
    public Camino darCaminoMasBarato( K idVerticeOrigen, K idVerticeDestino ) throws Exception
    {
        // Borra todas las marcas presentes en el grafo
        reiniciarMarcas( );
        // Obtiene los v�rtices
        Vertice verticeOrigen = darObjetoVertice( idVerticeOrigen );
        Vertice verticeDestino = darObjetoVertice( idVerticeDestino );
        // Le pide al v�rtice de origen que localice el camino
        return verticeOrigen.darCaminoMasBarato( verticeDestino );
    }

    /**
     * Indica si hay un ciclo en el grafo que pase por el v�rtice especificado
     * @param idVertice El identificador del v�rtice
     * @return <code>true</code> si existe el ciclo o <code>false</code> en caso contrario
     * @throws VerticeNoExisteException Si el v�rtice especificado no existe
     */
    public boolean hayCiclo( K idVertice ) throws Exception
    {
        // Borra todas las marcas presentes en el grafo
        reiniciarMarcas( );
        // Obtiene el v�rtice
        Vertice vertice = darObjetoVertice( idVertice );
        // Le pregunta al v�rtice de origen si a partir de �l hay un ciclo
        return vertice.hayCiclo( );
    }
    
    /**
     * Indica si el grafo es conexo
     * @return <code>true</code> si el grafo es conexo o <code>false</code> en caso contrario
     */
    public boolean esConexo( )
    {
        // Borra todas las marcas presentes en el grafo
        reiniciarMarcas( );
        return contarComponentesConexos( ) <= 1 ? true : false;
    }

    /**
     * Indica si el grafo es fuertemente conexo
     * @return true si el grafo es fuertemente conexo o false en caso contrario
     */
    public boolean esFuertementeConexo( )
    {
        for( Vertice v : vertices.values( ) )
        {
            for( Vertice v2 : vertices.values( ) )
                try
                {
                    if( v != v2 && !hayCamino( (K)v.darId( ), (K)v2.darId( ) ) )
                        return false;
                }
                catch( Exception e )
                {
                  
                }
        }
        return true;
    }
    
    public Lista darComponentesFuertementeConectadas() throws Exception
    {
    	Lista cfc = new Lista<>();
    	Lista l = darVertices();
    	for (int i = 0; i < l.size(); i++) {
			Vertice v1 = (Vertice)l.darElementoPosicion(i);
    		for (int j = 0; j < l.size(); j++) {
    			Vertice v2 = (Vertice)l.darElementoPosicion(j);	
    			if (hayCamino((K)v1.darId(), (K)v2.darId()) && hayCamino((K)v2.darId(), (K)v1.darId())) {
    				VOComponenteFuertementeConectada comp = new VOComponenteFuertementeConectada(v1, v2);
    				cfc.agregar(comp);
				}
    			
			}
		}
    	return cfc;
    }

    /**
     * Indica si en el grafo no hay ciclos
     * @return <code>true</code> si en el grafo no hay ciclos o <code>false</code> en caso contrario
     */
    public boolean esAciclico( )
    {
        // Recorrer todos los vertices
        for( Vertice vertice : vertices.values( ) )
        {
            try
            {
                reiniciarMarcas( );
                // Si se encuentra un ciclo el grafo no es aciclico
                if( hayCiclo( (K) vertice.darId( ) ) )
                    return false;
            }
            catch( Exception e )
            {
                // Esto nunca va a ocurrir
            }
        }
        return true;
    }

    

    /**
     * Indica si el grafo es completo. Un grafo es completo si existe un arco entre cualquier pareja de v�rtices en el grafo.
     * @return <code>true</code> si el grafo es completo o <code>false</code> en caso contrario
     */
    public boolean esCompleto( )
    {
        for( Vertice v : vertices.values( ) )
        {
            for( Vertice v2 : vertices.values( ) )
            {
                if( !v.darId( ).equals( v2.darId( ) ) && ! ( v.esSucesor( v2.darId( ) ) || v2.esSucesor( v.darId( ) ) ) )
                    return false;
            }
        }
        return true;
    }

    /**
     * Retorna el grafo central del grafo
     * @return El grafo central del grafo o <code>null</code> en caso de que todos su vértices tengan excentricidad infinita
     */
    public Vertice darCentro( )
    {
        int menorExcentricidad = 0;
        Vertice centro = null;

        for( Vertice vertice : vertices.values( ) )
        {
            // Calcular la excentricidad del vertice
            try
            {
                int excen = darExcentricidad( (K)vertice.darId( ) );

                if( centro == null && excen != INFINITO )
                {
                    centro = vertice;
                    menorExcentricidad = excen;
                }
                else if( excen != INFINITO && menorExcentricidad > excen )
                {
                    centro = vertice;
                    menorExcentricidad = excen;
                }
            }
            catch( Exception e )
            {
                // Esto no debería suceder
            }

        }
        return centro;
    }

    /**
     * Calcula la excentricidad de un v�rtice del grafo
     * @param idVertice Id del vertice
     * @return La excentricidad del v�rtice ingresado por par�metro, o Grafo.INFINITO en caso no poder alcanzar alguno de los v�rtices del grafo.
     * @throws VerticeNoExisteException Si el v�rtice buscado no existe
     */
    public int darExcentricidad( K idVertice ) throws Exception
    {
        CaminosMinimos<K, V, A> cm = dijkstra( idVertice );

        Integer mayorPeso = null;
        for( Vertice destino : vertices.values( ) )
        {
            // Obtener el costo del camino minimo de idVertice a destino
            int costo = cm.darCostoCamino( destino );

            if( costo == -1 )
                return INFINITO;
            if( mayorPeso == null )
                mayorPeso = costo;
            else if( mayorPeso < costo )
            {
                mayorPeso = costo;
            }
        }
        return mayorPeso.intValue( );
    }

    /**
     * El peso de un grafo es la suma de los pesos de todos sus arcos
     */
    public int darPeso( )
    {
        int peso = 0;
        Lista arcos = darArcos( );
        for( int i = 0; i < arcos.size( ); i++ )
        {
            peso +=  ((Arco)arcos.get( i )).darPeso( );
        }
        return peso;
    }

    /**
     * Retorna el �rbol parcial de recubrimiento del grafo que parte del v�rtice dado
     * @param idVertice El identificador del v�rtice
     * @return El �rbol de recubrimiento parcial del grafo que parte del v�rtice dado
     * @throws VerticeNoExisteException Si el vertice buscado no existe;
     */
    public GrafoDirigido darArbolParcialRecubrimiento( K idVertice ) throws Exception
    {
        Vertice vertice = darObjetoVertice( idVertice );
        reiniciarMarcas( );

        GrafoDirigido arbolPR = new GrafoDirigido( );
        vertice.darArbolParcialRecubrimiento( arbolPR );
        return arbolPR;
    }

    
    /**
     * Cuenta el n�mero de subgrafos que son conexos
     * @return El n�mero de grafos que son conexos
     */
    public int contarComponentesConexos( )
    {
        int compConexos = 0;

        reiniciarMarcas( );

        for( Vertice vertice : vertices.values( ) )
        {
            if( !vertice.marcado( ) )
            {
                vertice.marcarAdyacentes( );
                compConexos++;
            }
        }
        return compConexos;
    }
    
    
    /**
     * Devuelve el v�rtice identificado con el identificador especificado
     * @param idVertice Identificador del v�rtice
     * @return V�rtice buscado
     * @throws VerticeNoExisteException Excepci�n generada cuando el v�rtice buscado no existe en el grafo
     */
    public Vertice darObjetoVertice( K idVertice ) throws Exception
    {
        Vertice vertice = vertices.get( idVertice );
        if( vertice == null )
        {
            throw new Exception( "El v�rtice buscado no existe en el grafo");
        }
        return vertice;
    }

    /**
     * Devuelve el v�rtice identificado con el identificador especificado
     * @param idVertice Identificador del v�rtice
     * @return V�rtice buscado
     * @throws VerticeNoExisteException Excepci�n generada cuando el v�rtice buscado no existe en el grafo
     */
    public Vertice darVertice( K idVertice ) throws Exception
    {
        return (Vertice) darObjetoVertice( idVertice );
    }

    /**
     * Borra las marcas de todos los v�rtices del grafo
     */
    private void reiniciarMarcas( )
    {
        // Elimina todas las marcas presentes en los v�rtices del grafo
        for( Vertice vertice : vertices.values( ) )
        {
            vertice.desmarcar( );
        }
    }

}
