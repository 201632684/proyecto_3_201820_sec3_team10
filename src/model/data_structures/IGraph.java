package model.data_structures;

public interface IGraph<T> {

	int vertices();
	
	int arcos();
	
	 T[] darVerices();
	 
	 T[] darArcos();
	 
	 T get( T elem );
	 
	void add(T elem);
	
	void remove(T elem);
	
}
