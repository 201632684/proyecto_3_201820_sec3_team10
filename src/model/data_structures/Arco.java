package model.data_structures;

/**
 * Representa un arco del grafo
 * @param <K> Tipo del identificador de un v�rtice
 * @param <V> Tipo de datos del elemento del v�rtice
 * @param <A> Tipo de datos del elemento del arco
 */
public class Arco< O extends Comparable<O> , D extends Comparable<D> , I >
{
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * V�rtice desde el cual sale el arco
     */
    private Vertice origen;

    /**
     * V�rtice hacia el cual va el arco
     */
    private Vertice destino;

    /**
     * Elemento en el arco
     */
    private I infoArco;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Constructor del arco
     * @param pOrigen V�rtice desde el cual sale el arco
     * @param pDestino V�rtice hacia donde se dirige el arco
     * @param pPeso peso del arco
     */
    public Arco( Vertice pOrigen, Vertice pDestino, I pInfoArco )
    {
        origen = pOrigen;
        destino = pDestino;
        infoArco = pInfoArco;
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Devuelve el elemento del arco
     * @return Elemento en el arco
     */
    public I darInfoArco( )
    {
        return infoArco;
    }

    /**
     * Devuelve el v�rtice de destino del arco
     * @return v�rtice de destino del arco
     */
    public Vertice darVerticeDestino( )
    {
        return destino;
    }

    /**
     * Devuelve el v�rtice de origen del arco
     * @return v�rtice de origen del arco
     */
    public Vertice darVerticeOrigen( )
    {
        return origen;
    }

    /**
     * Devuelve el peso del arco
     * @return Peso del arco
     */
    public double darPeso( )
    {
        return infoArco.darPeso( );
    }
}
