package model.data_structures;

import model.data_structures.Lista;

/**
 * Representa un camino en un grafo
 * @param <K> Tipo del identificador de un v�rtice
 * @param <V> Tipo de datos del elemento del v�rtice
 * @param <A> Tipo de datos del elemento del arco
 */
public class Camino<K, V extends Comparable<V> , A extends Comparable<A>>
{
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Lista con los arcos del camino
     */
    private Lista arcos;

    /**
     * Lista con los v�rtices del camino
     */
    private Lista vertices;

    /**
     * Origen del camino
     */
    private V origen;

    /**
     * Costo total del camino
     */
    private int costo;

    // -----------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------

    /**
     * Constructor del camino
     * @param origen V�rtice de origen del camino
     */
    public Camino( V origen )
    {
        // Inicializar los atributos del camino
        vertices = new Lista( );
        arcos = new Lista( );
        costo = 0;
        this.origen = origen;
        
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    public Lista darArcos()
    {
    	return arcos;
    }
    
    public Lista darVertices()
    {
    	return vertices;
    }
    
    /**
     * Agrega un arco al final del camino
     * @param arco Arco a agregar
     */
    public void agregarArcoFinal( V vertice, A arco )
    {
        arcos.agregar( arco );
        vertices.agregar( vertice );
        costo += ((Arco)arco).darPeso( );
    }

    /**
     * Agrega un arco al comienzo del camino.
     * @param nuevoOrigen Nuevo origen del camino
     * @param arco Arco que va del nuevo origen al antiguo origen del camino
     */
    public void agregarArcoComienzo( V nuevoOrigen, A arco )
    {
        arcos.add( arco, 0 );
        vertices.add( origen, 0 );
        origen = nuevoOrigen;
        costo += ((Arco)arco).darPeso( );
    }

    /**
     * Concatena todos los arcos del camino especificado al final del camino
     * @param camino Camino a concatenar
     */
    public void concatenar( Camino<K, V, A> camino )
    {
        // Agregar los arcos y vertices del camino a concatenar ignorando el origen del camino ingresado por par�metro
        for( int i = 0; i < camino.arcos.size( ); i++ )
            agregarArcoFinal( (V)camino.vertices.get( i ), (A)camino.arcos.get( i ) );
    }

    /**
     * Elimina el �ltimo arco
     */
    public void eliminarUltimoArco( )
    {
        if( arcos.size( ) >= 1 )
        {
            A arco = (A)arcos.get( arcos.size( ) - 1 );
            arcos.eliminar( arcos.size( ) - 1 );
            vertices.eliminar( vertices.size( ) - 1 );
            costo -= ((Arco)arco).darPeso( );
        }
    }

    /**
     * Reinicia el camino conservando el origen
     */
    public void reiniciar( )
    {
        arcos.vaciar( );
        vertices.vaciar( );
        costo = 0;
    }

    /**
     * Devuelve la longitud del camino
     * @return Longitud del camino
     */
    public int darLongitud( )
    {
        return arcos.size( );
    }

    /**
     * Devuelve el costo del camino
     * @return Costo del camino
     */
    public int darCosto( )
    {
        return costo;
    }

    /**
     * Retorna el origen del camino
     * @return El vertice desde el que se origina el camino
     */
    public V darOrigen( )
    {
        return origen;
    }
}
