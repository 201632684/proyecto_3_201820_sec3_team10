package model.logic;

import API.IDivvyTripsManager;
import model.data_structures.Arco;
import model.data_structures.GrafoDirigido;
import model.data_structures.IGraph;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.data_structures.Vertice;
import model.vo.VOComponenteFuertementeConectada;
import model.vo.VOPath;
import model.vo.VOStation;
import model.vo.VOTrip;

import com.fasterxml.jackson.core;

public class DivvyTripsManager implements IDivvyTripsManager {

	public final static String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1";
	
	public final static String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q2";
	
	public final static String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3";
	
	public final static String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q4";
	
	public final static String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2";
	
	public final static String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4";
	
	public final static String MAPA_CHICAGO = "./data/MapaChicago";
	
	private Lista estaciones;
	
	private Lista trips;
	
	private Lista calles;
	
	private GrafoDirigido grafoChicago;
	
	
	public void cargarSistema() {
		// TODO Auto-generated method stub
		
		
		
	}

	public VOPath A1_menorDistancia(double latInicial, double lonInicial, double latFinal, double lonFinal) {
		//TODO Visualizacion Mapa
		VOPath path = null;
		VOStation estacionInicial = darEstacionMasCercana(latInicial, lonInicial);
		VOStation estacionFinal = darEstacionMasCercana(latFinal, lonFinal);
		try {
			path = new VOPath(grafoChicago.darCaminoMasCorto(estacionInicial, estacionFinal));
		} 
		catch (Exception e) 
		{
			// No deber�a suceder
			
		}
		
		return path;
	}

	public VOPath A2_menorNumVertices(double latInicial, double lonInicial, double latFinal, double lonFinal) {
		//TODO Visualizacion Mapa
		VOPath path = null;
		VOStation estacionInicial = darEstacionMasCercana(latInicial, lonInicial);
		VOStation estacionFinal = darEstacionMasCercana(latFinal, lonFinal);
		try {
			path = new VOPath(grafoChicago.darCaminoMasBarato(estacionInicial, estacionFinal));
		} catch (Exception e) {
			// No deber�a suceder
			
		}
		
		return path;
	}

	public IList<VOStation> B1_estacionesCongestionadas(int n) {
		//TODO Visualizacion Mapa
		return null;
	}

	public IList<VOPath> B2_rutasMinimas(IList<VOStation> stations) {
		//TODO Visualizacion Mapa
		return null;
	}

	public IGraph C1_grafoEstaciones()	{
		//TODO Visualizacion Mapa
		GrafoDirigido g = new GrafoDirigido<>();
		for (int i = 0; i < estaciones.size(); i++) {
			VOStation s = (VOStation)estaciones.darElementoPosicion(i);
			Vertice v = new Vertice(s);
			try {
				g.agregarVertice(v);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		for (int i = 0; i < trips.size(); i++) {
			VOTrip t = (VOTrip)trips.darElementoPosicion(i);
			Arco a = new Arco(t.darEstacionInicio(), t.darEstacionFin(), t.darInfoTrip());
			try {
				g.agregarArco(t.darEstacionInicio(), t.darEstacionFin(), a);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		IGraph ret = (IGraph)g;
		return ret;
	}
	
	public void C1_persistirGrafoEstaciones(IGraph grafoEstaciones){
		//TODO Visualizacion Mapa
		final ObjectMapper mapper = new ObjectMapper();
		final String json = mapper.WritevalueAsString(grafoEstaciones);
	}

	public IList<VOComponenteFuertementeConectada> C2_componentesFuertementeConectados(IGraph grafo) {
		//TODO Visualizacion Mapa
		Lista l = new Lista<>();
		GrafoDirigido grafoDirigidoComponentes = new GrafoDirigido<>(grafo);
		try {
			l = grafoDirigidoComponentes.darComponentesFuertementeConectadas();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		return l;
	}

	public void C3_pintarGrafoEstaciones(IGraph grafoEstaciones) {
		//TODO Visualizacion Mapa!
		
	}
	
	public VOStation darEstacionMasCercana(double latitud, double longitud)
	{
		VOStation masCercana = (VOStation) estaciones.darElementoPosicion(0);
		double distanciaMasCercana = Math.sqrt(   (Math.abs(((VOStation) estaciones.darElementoPosicion(0)).darLongitud()-longitud))*(Math.abs(((VOStation) estaciones.darElementoPosicion(0)).darLongitud()-longitud))  +  Math.abs((((VOStation) estaciones.darElementoPosicion(0)).darLatitud()-latitud))*Math.abs((((VOStation) estaciones.darElementoPosicion(0)).darLatitud()-latitud))  );
		double distanciaActual;
		
		for (int i = 1; i<estaciones.size(); i++ ) {
			VOStation s = (VOStation) estaciones.darElementoPosicion(i);
			distanciaActual = Math.sqrt( (Math.abs(s.darLongitud()-longitud))*(Math.abs(s.darLongitud()-longitud)) + Math.abs((s.darLatitud()-latitud))*Math.abs((s.darLatitud()-latitud)));
			if (distanciaActual<distanciaMasCercana) {
				distanciaMasCercana = distanciaActual;
				masCercana = s;
			}
		}
		return masCercana;
	}
}
