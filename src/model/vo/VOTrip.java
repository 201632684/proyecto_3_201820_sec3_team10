package model.vo;

import model.data_structures.Vertice;

public class VOTrip<I>
{
	private Vertice estacionInicio;
	
	private Vertice estacionFin;
	
	private I infoTrip;
	
	public VOTrip()
	{
		
	}
	
	public Vertice darEstacionInicio() {
		return estacionInicio;
	}
	
	public Vertice darEstacionFin() {
		return estacionFin;
	}
	
	public I darInfoTrip() {
		return infoTrip;
	}
}
