package model.vo;

import model.data_structures.Camino;
import model.data_structures.Lista;

public class VOPath implements Comparable<VOPath>
{
	
	//Definir un atributo lista de nodos o HashTable de nodos o cualquier otra forma que les permita 
	//--- almacenar la informacion
	
	//Definir un atributo distancia total de un camino(path)
	
	private Lista nodos;
	
	private Camino camino;
	
	private int distancia;

	private VOStation estacionOrigen;
	
	private VOStation estacionDestino;
	
	
	public VOPath(Camino c)
	{
		camino = c;
		distancia = camino.darLongitud();
		nodos = c.darArcos();
		estacionOrigen = (VOStation)c.darOrigen();
		Lista l = c.darVertices();
		estacionDestino = (VOStation) l.darElementoPosicion(l.size());
	}
	
	public int darDistancia()
	{
		return distancia;
	}
	public Camino darCamino() {
		return camino;
	}
	
	public VOStation darEstacionOrigen()
	{
		return estacionOrigen;
	}
	
	public VOStation darEstacionDestino()
	{
		return estacionDestino;
	}
	
	public int compareTo(VOPath o) 
	{
		//recomendamos comparar paths por distancia total.
		if (o.darDistancia()<distancia) {
			return -1;
		}
		else if (o.darDistancia()>distancia) {
			return 1;
		}
		
		return 0;
	}

}
