package model.vo;

import model.data_structures.Vertice;

public class VOComponenteFuertementeConectada implements Comparable<VOComponenteFuertementeConectada>{

	private Vertice inicio;
	
	private Vertice fin;
	
	public int compareTo(VOComponenteFuertementeConectada o) {
		return 0;
	}
	
	public VOComponenteFuertementeConectada(Vertice pInicio, Vertice pFin)
	{
		inicio = pInicio;
		fin = pFin;
	}
	

}
