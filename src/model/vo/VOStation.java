package model.vo;

import model.data_structures.Lista;

public class VOStation implements Comparable<VOStation>{

	
	private double latiutud;
	
	private double longitud;
	
	public double darLongitud()
	{
		return longitud;
	}
	public double darLatitud()
	{
		return latiutud;
	}
	public int compareTo(VOStation o) {
		if (o.equals(this)) {
			return 0;
		}
		return -1;
	}

}
