package controller;

import API.IDivvyTripsManager;
import model.vo.VOComponenteFuertementeConectada;
import model.vo.VOPath;
import model.vo.VOStation;
import model.data_structures.IGraph;
import model.data_structures.IList;
import model.logic.DivvyTripsManager;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static IDivvyTripsManager manager =new DivvyTripsManager();

	//Carga El sistema
	public static void cargarSistema()
	{
		 manager.cargarSistema();
	}
	
	//A1
	public static VOPath A1_menorDistancia(double latInicial, double lonInicial, double latFinal, double lonFinal){
		return manager.A1_menorDistancia(latInicial,lonInicial,latFinal,lonFinal);
	}
	
	//A2
	public static VOPath A2_menorNumVertices(double latInicial, double lonInicial, double latFinal, double lonFinal){
		return manager.A2_menorNumVertices(latInicial,lonInicial,latFinal,lonFinal);
	}
	
	//B1
	public static IList<VOStation> B1_estacionesCongestionadas(int n){
		return manager.B1_estacionesCongestionadas(n);
	}

	//B2
	public static IList<VOPath> B2_rutasMinimas(IList<VOStation> estaciones){
		return manager.B2_rutasMinimas(estaciones);
	}
	
	//C1
	public static IGraph C1_grafoEstaciones(){
		IGraph grafoEstaciones = manager.C1_grafoEstaciones();
		manager.C1_persistirGrafoEstaciones(grafoEstaciones);
		return grafoEstaciones;
	}
	
	//C2
	public static IList<VOComponenteFuertementeConectada> C2_componentesFuertementeConectados(IGraph grafo){
		return manager.C2_componentesFuertementeConectados(grafo);
	}
	
	//C3
	public static void C3_pintarGrafoEstaciones(IGraph grafoEstaciones){
		manager.C3_pintarGrafoEstaciones(grafoEstaciones);
	}
}
